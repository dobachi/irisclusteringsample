package com.dobachi.spark.sample

case class IrisClustringSampleConfig(inputPath: String = "",
                                outputPath: String ="",
                                numClusters: Int = 3,
                                iterNum: Int = 10)
/**
 * Option parser
 */
@SerialVersionUID(1L)
class IrisClustringSampleOpParser() extends Serializable{
  val parser = new scopt.OptionParser[IrisClustringSampleConfig]("SushiRecommend") {
    arg[String]("inputPath") required() action {
      (x, c) => c.copy(inputPath = x)
    } text("Path of input data")

    arg[String]("outputPath") required() action {
      (x, c) => c.copy(outputPath = x)
    } text("Path of output data")

    arg[Int]("numClusters") required() action {
      (x, c) => c.copy(numClusters = x)
    } text("Number of clusters")

    opt[Int]('i', "iterNum") valueName("<int number>") action {
      (x, c) => c.copy(iterNum = x)
    } text("Number of iteration")
   }

  def getParser() = {
    parser
  }
}

object IrisClustringSampleOpParser {
  val sushiClusteringOpParser = new IrisClustringSampleOpParser()

  def apply() = {
    sushiClusteringOpParser
  }
}

